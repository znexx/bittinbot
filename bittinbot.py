from sopel import module, tools, plugins
from string import Template, capwords
import random

# victim (passivt verb) bully |(location) |(sidestory) |(emote)
# bully (aktivt verb) victim |(location) |(sidestory) |(emote)
# victim (passivt soloverb) |(location) |(sidestory) |(emote)

# (victim) (action) (bully) |(location) |(sidestory) |(emote)
# (victim) (soloaction) |(location) |(sidestory) |(emote)

data = {}

actionpatterns = {
    'solo_verb': '$victim $action',
    'passive_verb': '$victim $action $bully',
    'active_verb': '$bully $action $victim'
}

non_insult_lists = [
    'pronouns',
    'ramblings'
]

def flip_coin():
    return random.choice([True, False])

def get_pronoun():
    return capwords(random.choice(data['pronouns']))

def get_optional(insult_data, optional_type):
    if optional_type in insult_data:
        return insult_data.pop(optional_type)
    elif optional_type not in data.keys():
        raise Exception("Tried to get a non-supplied non-standard type of insult part")
    elif flip_coin():
        return random.choice(data[optional_type])
    else:
        return None

def maybe_append(insult, addon):
    if addon:
        if not addon.startswith(","):
            insult += " "
        insult += addon
    return insult

def make_insult(insult_data):
    if not 'victim' in insult_data:
        raise Exception("Trying to make an insult without a victim, huh?")

    victim = insult_data.pop('victim')

    # checks if we have provided some action type in insult_data, else we get an empty list
    provided = list(set(actionpatterns.keys()) & set(insult_data.keys()))

    # gets a action type if some action type was specified in insult_data, else a random one
    actiontype = provided[0] if provided else random.choice(list(actionpatterns.keys()))

    if not actiontype in data:
        raise Exception('Action type ' + actiontype + ' is missing in the global data scope lol')

    # gets the action verb part from the specified in the insult_data, else a random one
    action = insult_data.pop(actiontype, random.choice(data[actiontype]))

    # checks if we need a bully, and raises an exception if we got none
    bully = insult_data.pop('bully', None)
    if '$bully' in actionpatterns[actiontype] and bully == None:
        raise Exception("Trying to make an insult that needs a bully without a bully")

    insult = Template(actionpatterns[actiontype]).substitute(victim=victim, action=action, bully=bully)

    insult = maybe_append(insult, get_optional(insult_data, 'location'))
    insult = maybe_append(insult, get_optional(insult_data, 'sidestory'))
    insult = maybe_append(insult, get_optional(insult_data, 'emote'))

    return insult

def setup(bot):
    global data
    default_data = {
        'solo_verb': [
        ],
        'passive_verb': [
        ],
        'active_verb': [
        ],
        'location': [
        ],
        'sidestory': [
        ],
        'emote': [
        ],
        'pronouns': [
            u'idiot'
        ],
        'ramblings': [
        ]
    }
    stored_data = bot.db.get_plugin_value('bittinbot', 'insult_store')
    if stored_data:
        for category in default_data.keys():
            if not category in stored_data:
                stored_data[category] = []
    else:
        # stored data was empty, resetting db...
        stored_data = default_data
    bot.db.set_plugin_value('bittinbot', 'insult_store', stored_data)
    data = stored_data

@module.nickname_commands('add\s+(\S+)\s+(.+)')
def addevent(bot, trigger):
    global data
    datatype = trigger.group(2)
    if datatype in data:
        value = trigger.group(3)
        if not value in data[datatype]:
            data[datatype].append(value)
            bot.db.set_plugin_value('bittinbot', 'insult_store', data)
            if datatype in non_insult_lists:
                bot.say(value + ' tillagd i mina ' + datatype)
            else:
                bot.say(make_insult({'victim': trigger.nick, 'bully': bot.nick, datatype: value}))
        else:
            bot.reply(get_pronoun() + ", \"" + value + "\" finns ju redan som " + datatype)
    else:
        bot.reply(get_pronoun() + ", " + datatype + u' finns inte som typ, prova n\u00e5n av dessa: ' + ', '.join(data.keys()))

@module.nickname_commands('remove\s+(\S+)\s+(.+)')
def removeevent(bot, trigger):
    global data
    datatype = trigger.group(2)
    if datatype in data:
        value = trigger.group(3)
        if value in data[datatype]:
            data[datatype].remove(value)
            bot.db.set_plugin_value('bittinbot', 'insult_store', data)
            bot.reply("Tog bort \"" + value + u'\" fr\u00e5n mina ' + datatype)
        else:
            bot.reply(get_pronoun() + ", \"" + value + "\" finns inte bland mina " + datatype)
    else:
        bot.reply(get_pronoun() + ", " + datatype + u' finns inte som typ! Prova n\u00e5n av dessa: ' + ', '.join(data.keys()))

@module.nickname_commands('list\s+(\S+)')
def listevents(bot, trigger):
    def list_category(category):
        if category in data:
            bot.say("Category " + category)
            for value in data[category]:
                bot.say("\"" + value + "\"")
        else:
            bot.reply(get_pronoun() + ", " + category + u' finns inte som typ, prova n\u00e5n av dessa: ' + ', '.join(data.keys()))
    global data
    datatype = trigger.group(2)
    if datatype == 'all':
        for dt in data.keys():
            list_category(dt)
    elif datatype == 'users':
        channel = bot.channels[trigger.sender]
        for user in channel.users:
            bot.say("user: " + user, "z-nexx")
    else:
        list_category(datatype)

@module.nickname_commands('m+[0ouå]+[8b]{2,}[4a]+h*\s+(\S+)')
def mubba(bot, trigger):
    bully = trigger.nick
    victim = trigger.group(2)

    if trigger.sender in bot.channels:
        channel = bot.channels[trigger.sender]

        for user in channel.users:
            if victim.casefold() == user.casefold():
                bot.say(make_insult({'victim': victim, 'bully': bully}))
                break
        else:
            bot.reply("Kan inte hitta " + victim + " i kanalen! ")
            bot.say(make_insult({'victim': bully, 'bully': bot.nick}))
    else:
        pass
        # sent not in a channel
        #bot.say("no find mister here pls")

@module.interval(60)
def say_something_stupid(bot):
    if random.random() < 0.0002:
        bot.say(random.choice(data['ramblings']), "#bastuklubben")

@module.nickname_commands('h+[ae]+i+l+')
def ramble(bot, trigger):
    bot.say(random.choice(data['ramblings']))
